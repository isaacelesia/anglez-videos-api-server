const express = require('express');
const router = express.Router();
const ApiService = require('../services/ApiService');
const {
	YOUTUBE_API_KEY,
	TWITCH_CLIENT_ID,
	TWITCH_TOKEN,
} = require('../config');

//@route   GET /api/videos/youtube
//@desc    Get videos from youtube
//@access  private
router.get('/youtube', (req, res, next) => {
	const parameters = req.query;

	if (!parameters)
		return res.status(400).json({
			error: { message: 'Missing parameters' },
		});

	const baseURL = 'https://www.googleapis.com/youtube/v3/search?';

	ApiService.getItems(baseURL, { ...parameters, key: YOUTUBE_API_KEY })
		.then((result) => res.status(200).json(result.data))
		.catch(next);
});

//@route   GET /api/videos/twitch
//@desc    stream videos from twitch
//@access  private
router.get('/twitch', (req, res, next) => {
	const parameters = req.query;

	if (!parameters)
		return res.status(400).json({
			error: { message: 'Missing parameters' },
		});

	//Generate twitch OAuth2 token
	ApiService.twitchOAuth().then((result) => console.log(result));

	const baseURL = 'https://api.twitch.tv/helix/streams?';
	const header = {
		'client-id': TWITCH_CLIENT_ID,
		Authorization: ` Bearer ${TWITCH_TOKEN}`,
	};

	ApiService.getItems(baseURL, parameters, header)
		.then((result) => res.status(200).json(result.data))
		.catch(next);
});

module.exports = router;
