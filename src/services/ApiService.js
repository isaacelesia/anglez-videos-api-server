const axios = require('axios');
const { TWITCH_CLIENT_ID, TWITCH_CLIENT_SECRET } = require('../config');

const ApiService = {
	getItems(baseURL, params = {}, header = {}) {
		return axios({
			method: 'get',
			url: baseURL,
			params: params,
			headers: header,
		})
			.then((res) => res)
			.catch((err) => console.error(err));
	},

	twitchOAuth() {
		return axios({
			method: 'post',
			url: 'https://id.twitch.tv/oauth2/token?',
			params: {
				client_id: TWITCH_CLIENT_ID,
				client_secret: TWITCH_CLIENT_SECRET,
				grant_type: 'client_credentials',
			},
		})
			.then((res) => res.data)
			.catch((err) => console.error(err));
	},
};

module.exports = ApiService;
