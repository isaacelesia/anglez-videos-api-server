module.exports = {
	PORT: process.env.PORT || 8000,
	NODE_ENV: process.env.NODE_ENV || 'development',
	YOUTUBE_API_KEY: process.env.YOUTUBE_API_KEY,
	TWITCH_CLIENT_ID: process.env.TWITCH_CLIENT_ID,
	TWITCH_CLIENT_SECRET: process.env.TWITCH_CLIENT_SECRET,
	TWITCH_TOKEN: process.env.TWITCH_TOKEN,
};
