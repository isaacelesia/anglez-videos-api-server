# Third party API’s

This server goal is to get videos through third party API's

## Package installation

Install packages `npm i`

## Scripts

Start the application `npm start`

Start the application in devlopment `npm run dev`

Run the tests `npm test`

## Set up environmental variables

1. YOUTUBE_API_KEY: Get YOUTUBE_API_KEY from [https://console.developers.google.com/apis/dashboard](https://console.developers.google.com/apis/dashboard)
2. TWITCH_CLIENT_ID
3. TWITCH_CLIENT_SECRET
4. TWITCH_TOKEN: Generate twitch OAuth2 token by un-commenting ApiService.twitchOAuth() function in the twitch file. Copy and save the generated OAuth2 token from the console.

## Deploying

When your new project is ready for deployment, add a new Heroku application with `heroku create`. This will make a new git remote called "heroku" and you can then `npm run deploy` which will push to this remote's master branch.
